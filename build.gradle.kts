import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("maven")
    val kotlinVersion = "1.3.21"
    id("org.springframework.boot") version "2.1.2.RELEASE"
    id("org.jetbrains.kotlin.jvm") version kotlinVersion
    id("io.spring.dependency-management") version "1.0.6.RELEASE"
    id("org.jetbrains.kotlinx.kover") version "0.4.2"
}

version = "1.0.0-SNAPSHOT"
var artifactId = "kotlin-sample-jar"
var groupId = "arcesium"
var application = "kotlin-sample-jar"

if (project.hasProperty("releaseVersion")) {
    version = project.property("releaseVersion")!!
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
        freeCompilerArgs = listOf("-Xjsr305=strict")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.bootJar {
    enabled = false
}

tasks.jar {
    enabled = true
}

java {
    withSourcesJar()
}

tasks.test {
    useJUnitPlatform()
}

task("writePom") {
    doLast() {
        project.maven.pom() {}.writeTo("$buildDir/pom.xml")
    }
}

tasks.koverVerify {
    rule {
        name = "Minimal line coverage rate in percents"
        bound {
            minValue = 90
        }
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("junit:junit:4.13.1")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}
